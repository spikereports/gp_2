# Spike Report

## Graphic Programming - Particle Systems

### Introduction

This spike teaches how to use the particle system and create 4 different types of particle systems.

### Goals

1. The Spike Report should answer each of the Gap questions
    1. Each particle system created should be marked as mobile-friendly, or not mobile-friendly based on the guidelines from the mobile content creation guide 
        (don’t forget to check that the materials work)
    1. Test using the Mobile Previewer

1. For each of the following non-technical particle briefs/requests, come up with a short plan, and then build the particle system.
    1. Rocket Launcher exhaust (the particle that comes out of a rocket launcher as you fire it)
    1. A snow or rain particle system which can be attached to the player (camera) to simulate weather effects.
    1. A red-to-yellow coloured mining laser beam, with “charging up” spikes at the start of the beam.
    1. A world war 1 fragmentation grenade effect using the Mesh type.

1. Use each of the following particle modules at least once. You can either include them in the above particles, or create additional particles to fulfil these requirements:
    1. Colour Modules
    1. Size modules
    1. Spawn Modules
    1. Velocity Modules
    1. Acceleration Modules
    1. Collision Modules

### Personnel

In this section, list the primary author of the spike report, as well as any personnel who assisted in completing the work.

* Primary - Bradley
* Secondary - N/A

### Technologies, Tools, and Resources used

Provide resource information for team members who wish to learn additional information from this spike.

1. RainFall Particle System
    * [Tutorial – Rain Particle System Unreal engine 4](https://nowyoshi.com/2016/10/09/tutorial-rain-particle-system-unreal-engine-4/)
    * [Unreal Engine 4 - Rain Particles | Ground wetness](https://www.youtube.com/watch?v=2B8TdZyBApY).
    * [UE4 - Tutorial - Simple Rain Particles](https://www.youtube.com/watch?v=Qmb2FrFunME)

1. Rocket Launcher Exhaust
    * [UE4 tutorial: Smoke particle system](https://www.youtube.com/watch?v=IAaBSuj1Bi8)
    * [Unreal Engine 4: Part 4 – Particle smoke](https://vimeo.com/149162343)

1. Grenade Explosion
    * [Grenade Mesh](https://www.turbosquid.com/3d-models/mk2-grenade-fbx-free/1042250)
    * [Merging into one actor](https://answers.unrealengine.com/questions/123763/combining-static-mesh-in-editor.html)

### Tasks Undertaken

#### Rainfall
1. Create a new folder that will hold all the rainfall blueprints.

1. Create new material called rainfall with a base color, particle color and RadialGradientExponential (RGE) and change to translucent and two sided.
    1. Have 2 multiply nodes that has the particle color RBG and RGE into one multiply and insert it into the emissive color.
    1. Insert the particle color alpha and RGE into the other multiply and insert into the opacity.
    1. Create a vector color for the base color, change the color to a light blue.

1. Create a new particle system and change its type to GPU sprites. Remove colour over life and add initial color, initial location, size by speed, const acceleration and collision. Adjust where necessary.
    1. In Required add the rainfall material
    1. Lifetime, change colour to light blue and set min and max to 5
    1. Initial size max 2, 2, 2. min 2,2, -10
    1. Initial velocity min 0, 0 , -500
    1. Initial Color light blue
    1. Initial Location max 200, 200, 200. Min 0, 0, -200
    1. Size by speed Speed scale 0, 5 max scale 0.2, 5.0
    1. Const Acceleration 0, 0 ,-400
    1. Collision Friction 0.2 and response to kill.

1. Drag the rainfall particle into the scene and watch it rain.

#### Rocket Launcher Exhaust 

1. Create a material with a smoke texture, particle color and a lerp and set the material to a mask blend.

1. Create a new particle system called P_RocketLauncherExhaust with two particle emitters that use the new material.
1. Add initial color to the first emitter and change the colours to red and black.
1. Adjust the spawn times to fit the effect thats needed.
1. Adjust the lifetime to the necessary values for the correct effect.
1. Allow both emitters to be destroyed/killed after completed.

#### Grenade Explosion

1. Download the free 3D mesh from resources used and merge the individual pieces into one actor.
    1. Editor preferences -> search for merge actor and set up keybind.
    1. Select the grenade meshes and merge them together.

1. Create a new particle systems called P_GrenadeExplosion and either rocks or the grenade itself explode when the emitter is called..
    1. Create a emitter that will show smoke (can use the same setting from the rocket Launcher).
    1. Another emitter that will launch a mesh in all different directions.

1. Create a new blueprint with the grenade mesh and after a few seconds spawns the emitters at the grenades location and rotation.
    1. Apply phyics to the grenade.
    1. Drag into the scene with some height to allow it to drop and then explode.

#### Beam Weapon

1. Create a new material with the particle color node into the base color.
    1. plan white image will work

1. Create a new particle System called P_Beam.
    1. Set its velocity to go straight up.
    1. Have the initial color set tp read.
    1. Adjust the spawn constant to whats seem fit
    1. Add the color of time and go from Red to Yellow.
        1. have 4 different points in the array that go from 0, .5 , .75, 1.

1. Drag into the scene and watch the beam fly.

### What we found out

1. The downloaded model from the resources used was in pieces. Learnt how to take all the individual pieces and merge them into one actor.


1. Particle systems can have a multiple of effects inside them and can be spread in different directions. 

1. Creating materials needed for the particles

### Open Issues/Risks

1. It seems that the beam can be very costly with how much it emits,

### Recommendations

1. Having the correct textures for the different particles 